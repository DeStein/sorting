#include "bubblesort.h"

void bubblesort(int numbers[], int length){
    for(int i=0;i<length-1;i++){
        for(int j=0;j<length-i-1;j++){
            //Change numbers if the next one is smaller
            if(numbers[j]>numbers[j+1]){
                int help=numbers[j+1];
                numbers[j+1]=numbers[j];
                numbers[j]=help;
            }
        }
    }
    return;
}