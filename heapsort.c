#include "heapsort.h"


void maxheapify(int numbers[], int pos, int *length){
    int l=pos*2+1;
    int r=pos*2+2;
    int max;

    if(l<=*length-1&&numbers[l]>numbers[pos]){
        max = l;
    }else{
        max=pos;
    }
    if(r<=*length-1&&numbers[r]>numbers[max]){
        max = r;
    }
    if(max!=pos){
        int help=numbers[pos];
        numbers[pos]=numbers[max];
        numbers[max]=help;
        maxheapify(numbers, max, length);
    }
}

void buildmaxheap(int numbers[], int *length){
    int heap=*length-1;
    for(int i=heap/2;i>=0;i--){
        maxheapify(numbers, i, length);
    }
}

int heapextractmax(int numbers[], int *length){
    int m=numbers[0];
    numbers[0]=numbers[*length-1];
    *length=*length-1;
    maxheapify(numbers, 0, length);
    return m;
}

void heapsort(int numbers[], int length, int sorted[]){
    int heapsize=length;
    buildmaxheap(numbers, &heapsize);
    for(int i=length-1;i>=0;i--){
        sorted[i]=heapextractmax(numbers, &heapsize);
    }
}