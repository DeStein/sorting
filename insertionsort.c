#include "insertionsort.h"

void insertionsort(int numbers[], int length){
    for(int i=1; i<length; i++){
        int key = numbers[i];
        int j = i-1;
        //Go left in the array until numbers is bigger than the selected element
        while(j>=0 && numbers[j]>key){
            numbers[j+1] = numbers[j];
            j--;
        }

        numbers[j+1]=key;
    }
    return;
}