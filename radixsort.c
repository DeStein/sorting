#include "radixsort.h"
#include <math.h>

void radixsort(int numbers[], int length){
    int b = 10;
    int d = 10;
    int L[b][length];
    for(int i=0;i<b;i++){
        for(int ii=0;ii<length;ii++){
            L[i][ii]=0;
        }
    }
    
    for(int i=1;i<=d;i++){
        int zero=0;
        int one=0;
        int two=0;
        int three=0;
        int four=0;
        int five=0;
        int six=0;
        int seven=0;
        int eight=0;
        int nine=0;
        for(int ii=0;ii<length;ii++){
            int help=numbers[ii]%((int)(pow(10,i)))/(int)(pow(10,i-1));
            //printf("%d, %d\n", help,(int)(pow(10,i)));
            switch (help){
            case 0:
                L[0][zero]=numbers[ii];
                zero++;

                break;
            case 1:
                L[1][one]=numbers[ii];
                one++;

                break;
            case 2:
                L[2][two]=numbers[ii];
                two++;

                break;
            case 3:
                L[3][three]=numbers[ii];
                three++;

                break;
            case 4:
                L[4][four]=numbers[ii];
                four++;

                break;
            case 5:
                L[5][five]=numbers[ii];
                five++;

                break;
            case 6:
                L[6][six]=numbers[ii];
                six++;

                break;
            case 7:
                L[7][seven]=numbers[ii];
                seven++;

                break;
            case 8:
                L[8][eight]=numbers[ii];
                eight++;

                break;
            case 9:
                L[9][nine]=numbers[ii];
                nine++;

                break;
            
            default:
                printf("Numbers not matching Buckets!!! Number is %d", help);

                break;
            }
        }
        int bucket=0;
        int num=0;
        int done=0;
        while(done<length){
            if(L[bucket][num]!=0){
                numbers[done]=L[bucket][num];
                L[bucket][num]=0;
                num++;
                done++;
            }else{
                bucket++;
                num=0;
            }
        }
    }
}