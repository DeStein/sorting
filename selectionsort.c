#include "selectionsort.h"

void selectionsort(int numbers[], int length){
    for(int i=0; i<length-1;i++){
        int min = i;
        for(int j=i+1;j<length;j++){
            if(numbers[j]<numbers[min]){
                min = j;
            }
        }
        int help = numbers[min];
        numbers[min] = numbers[i];
        numbers[i] = help;
    }
    return;
}