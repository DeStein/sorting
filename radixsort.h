#ifndef RADIXSORT_H
#define RADIXSORT_H

#include <stdio.h>
#include <math.h>

void radixsort(int numbers[], int length);

#endif