#include "quicksort.h"

int partition(int numbers[], int lo, int hi){
    int pivot=numbers[(hi+lo)/2];
    int i=lo-1;
    int j=hi+1;
    while(1){
        do
        {
            i++;
        } while (numbers[i]<pivot);
        
        do
        {
            j--;
        } while (numbers[j]>pivot);

        if(i>=j){
            return j;
        }
        
        int help = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = help;
    }
}

void sortquick(int numbers[], int lo, int hi){
    if(lo<hi){
        int pivot = partition(numbers, lo, hi);

        sortquick(numbers, lo, pivot);
        sortquick(numbers, pivot+1, hi);
    }
}


void quicksort(int numbers[], int length){
    sortquick(numbers, 0, length-1);
}