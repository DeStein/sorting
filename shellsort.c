#include "shellsort.h"

void shellsort(int numbers[], int length){
    int gap[]={1,5,19,41,109,209,505,929,2161,3905,8929,16001,36289,64769,146305,260609,587521};
    int gaplength=17;
    for(int i=gaplength-1;i>=0;i--){
        for(int ii=gap[i];ii<length;ii++){
            int temp=numbers[ii];
            int iii;
            for(iii=ii;iii>=gap[i]&&numbers[iii-gap[i]]>temp;iii-=gap[i]){
                numbers[iii]=numbers[iii-gap[i]];
            }
            numbers[iii]=temp;
        }
    }
}