#include "combsort.h"

void combsort(int numbers[], int length){
    int gap=length;
    int shrink=13;
    int swapped=1;
    while(gap!=1||swapped){
        if(gap<1){
            gap=1;
        }else{
            gap=(gap*10)/shrink;
        }
        swapped=0;
        for(int i=0;i<length-gap;i++){
            if(numbers[i]>numbers[i+gap]){
                int help=numbers[i];
                numbers[i]=numbers[i+gap];
                numbers[i+gap]=help;
                swapped=1;
            }
        }
    }
}