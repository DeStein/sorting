#ifndef HEAPSORT_H
#define HEAPSORT_H

#include <stdio.h>

void heapsort(int numbers[], int length, int sorted[]);

#endif