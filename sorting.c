#include "sorting.h"

int main(){
    //Vars
    int length = 0;
    char str_buf[20];

    //Input how many numbers to generate
    printf("How many numbers should be generated and sorted?\n");
    fgets(str_buf, sizeof str_buf, stdin);
    sscanf(str_buf, "%d", &length);

    //Create Random Number Arrays
    int numbers[length];
    int bubble[length];
    int insert[length];
    int select[length];
    int quick[length];
    int merge[length];
    int heap[length];
    int shell[length];
    int comb[length];
    int radix[length];

    //Random Number Generator
    srandom(time(0));

    for(int i=0; i<length; i++){
        numbers[i]=random();
    }

    //Copy Array
    for(int i=0; i<length; i++){
        bubble[i]=numbers[i];
        quick[i]=numbers[i];
        select[i]=numbers[i];
        insert[i]=numbers[i];
        merge[i]=numbers[i];
        heap[i]=numbers[i];
        shell[i]=numbers[i];
        comb[i]=numbers[i];
        radix[i]=numbers[i];
    }

    printf("Numbers succesfully generated!\n");

    //timer init
    clock_t start, stop;


    /*for(int i=0;i<length;i++){
        printf("%d\n", radix[i]);
    }*/


    //Bubblesort
    start = clock();
    //bubblesort(bubble, length);
    stop = clock();

    printf("Bubblesort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Insertionsort
    start = clock();
    //insertionsort(insert, length);
    stop = clock();

    printf("Insertionsort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Selectionsort
    start = clock();
    //selectionsort(select, length);
    stop = clock();

    printf("Selectionsort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Quicksort
    start = clock();
    quicksort(quick, length);
    stop = clock();

    printf("Quicksort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Mergesort
    start = clock();
    mergesort(merge, length);
    stop = clock();

    printf("Mergesort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Heapsort
    int sorted[length];
    start = clock();
    heapsort(heap, length, sorted);
    stop = clock();

    printf("Heapsort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Shellsort
    start = clock();
    shellsort(shell, length);
    stop = clock();

    printf("Shellsort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Combsort
    start = clock();
    combsort(comb, length);
    stop = clock();

    printf("Combsort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);


    //Radixsort
    start = clock();
    radixsort(radix, length);
    stop = clock();

    printf("Radixsort took %.10f sec. to sort %d elements.\n", (float) (stop-start)/CLOCKS_PER_SEC,length);



    /*for(int i=0;i<length;i++){
        printf("%d\n", radix[i]);
    }*/

}