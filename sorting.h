#ifndef SORTING_H
#define SORTING_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "bubblesort.h"
#include "insertionsort.h"
#include "selectionsort.h"
#include "quicksort.h"
#include "mergesort.h"
#include "heapsort.h"
#include "shellsort.h"
#include "combsort.h"
#include "radixsort.h"

#endif