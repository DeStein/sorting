#include "mergesort.h"

void merge(int numbers[], int lo, int m, int hi){
    int length = hi-lo+1;
    int help[length];
    int left = lo;
    int right = m+1;

    for(int i=0;i<length;i++){
        if(left>m){
            if(right<=hi){
                for(int j=right;j<=hi;j++){
                    help[i]=numbers[j];
                    i++;
                }
            }
            continue;
        }
        if(right>hi){
            if(left<=m){
                for(int j=left;j<=m;j++){
                    help[i]=numbers[j];
                    i++;
                }
            }
            continue;
        }
        if(numbers[left]<numbers[right]){
            help[i]=numbers[left];
            left++;
        }
        else if(numbers[left]>numbers[right]){
            help[i]=numbers[right];
            right++;
        }
    }
    int j=0;
    for(int i=lo;i<=hi;i++){
        numbers[i]=help[j];
        j++;
    }
}

void sortmerge(int numbers[], int lo, int hi){
    if(lo<hi){
        int m = (hi+lo)/2;
        sortmerge(numbers, lo, m);
        sortmerge(numbers, m+1, hi);
        merge(numbers, lo, m, hi);
    }
}

void mergesort(int numbers[], int length){
    sortmerge(numbers, 0, length-1);
}